#!/bin/bash

PJ_NAME=test
C_STD=99
CXX_STD=11
CMAKE_VERSION=3.8
QT_AVAILABLE=OFF
QT_QUICK_VERSION=2.12
GIT_IGNORE=OFF

if [[ "$1" == "--help" || "$1" == "-h" ]]
then
	echo "-p|--project-name set project name. Default is ${PJ_NAME}"
	echo "-c|--c-std set C standard. Default is ${C_STD}"
    echo "-cxx|--cxx-std set C++ standard. Default is ${CXX_STD}"
	echo "--cmake-version set version for cmake_minimum_required(). Default is ${CMAKE_VERSION}"
    echo "--qt must be ON if you want use as qt. Default is ${QT_AVAILABLE}"
	echo "--quick-version set version for qt quick module"
	echo "	Example: import QtQuick ${QT_QUICK_VERSION}"
	echo "	Default version is ${QT_QUICK_VERSION}"
	echo "--git-ignore is must ON if you want create .gitignore. Default is ${GIT_IGNORE}"
	exit
fi

for arg in "$@"
do
	case $arg in
	-p|--project-name)
        PJ_NAME=$2
        shift
        shift
    ;;
    -c|--c-std)
        C_STD=$2
        shift
        shift
    ;;
	-cxx|--cxx-std)
		CXX_STD=$2
		shift
        shift
	;;
    --cmake-version)
        CMAKE_VERSION=$2
        shift
        shift
    ;;
    --qt)
        QT_AVAILABLE=ON
        shift
        shift
    ;;
    --quick-version)
        if [ "${QT_AVAILABLE}" == "ON" ]
        then
            QT_QUICK_VERSION=$2
            shift
            shift
        fi
    ;;
    --git-ignore)
        GIT_IGNORE=$2
        shift
        shift
    ;;
	esac
done

if [ "${GIT_IGNORE}" == "ON" ]
then
	cat > .gitignore <<-EOF
.idea/
.vscode/
build/
	EOF
fi


if [ "${QT_AVAILABLE}" == "OFF" ]
then
    cat > CMakeLists.txt <<-EOF
cmake_minimum_required(VERSION ${CMAKE_VERSION} FATAL_ERROR)
project(${PJ_NAME} LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_C_STANDARD ${C_STD})
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD ${CXX_STD})
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(HEADER )
set(SRC main.cpp)

add_executable(\${PROJECT_NAME} \${HEADER} \${SRC})

	EOF
else
    cat > CMakeLists.txt <<-EOF
cmake_minimum_required(VERSION ${CMAKE_VERSION} FATAL_ERROR)
project(${PJ_NAME} LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_C_STANDARD ${C_STD})
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD ${CXX_STD})
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS Core Quick REQUIRED)

set(HEADER )
set(SRC )

if(ANDROID)
    add_library(\${PROJECT_NAME} SHARED
        \${HEADER}
        \${SRC}
        main.cpp
        qml.qrc
    )
else()
    add_executable(\${PROJECT_NAME}
        \${HEADER}
        \${SRC}
        main.cpp
        qml.qrc
    )
endif()

target_compile_definitions(\${PROJECT_NAME} PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(\${PROJECT_NAME} PRIVATE Qt5::Core Qt5::Quick)

	EOF
fi

if [ "${QT_AVAILABLE}" == "ON" ]
then
    cat > main.cpp <<-EOF
#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    const QUrl url(QStringLiteral("qrc:/main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                    &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}

	EOF
else
    cat > main.cpp <<-EOF
#include <iostream>

using std::cout;
using std::endl;

int main()
{
    cout << "Hello world" << endl;
    return 0;
}

	EOF
fi

if [ "${QT_AVAILABLE}" == "ON" ]
then
    cat > main.qml <<-EOF
import QtQuick ${QT_QUICK_VERSION}
import QtQuick.Window ${QT_QUICK_VERSION}

Window {
    width: 640
    height: 480
    visible: true
    title: "Hello world"
}

	EOF

    cat > qml.qrc <<-EOF
<RCC>
    <qresource prefix="/">
        <file>main.qml</file>
    </qresource>
</RCC>
	EOF
fi

mkdir build

cat > build.sh <<EOF
cmake -S . -Bbuild
cmake --build build
EOF

chmod +x build.sh
